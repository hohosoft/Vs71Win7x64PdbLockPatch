namespace Vs71Win7x64PdbLockPatch
{
	using System;
	using Microsoft.Office.Core;
	using Extensibility;
	using System.Runtime.InteropServices;
	using EnvDTE;
	using System.Globalization;
	using System.Windows.Forms;
	using System.Resources;
	using System.Reflection;
 
	#region Read me for Add-in installation and setup information.
	// When run, the Add-in wizard prepared the registry for the Add-in.
	// At a later time, if the Add-in becomes unavailable for reasons such as:
	//   1) You moved this project to a computer other than which is was originally created on.
	//   2) You chose 'Yes' when presented with a message asking if you wish to remove the Add-in.
	//   3) Registry corruption.
	// you will need to re-register the Add-in by building the MyAddin21Setup project 
	// by right clicking the project in the Solution Explorer, then choosing install.
	#endregion
	  
	/// <summary>
	///   The object for implementing an Add-in.
	/// </summary>
	/// <seealso class='IDTExtensibility2' />
	[GuidAttribute("79262BD5-5631-477A-9F0A-C6401F8458F5"), ProgId("Vs71Win7x64PdbLockPatch.Connect")]
	public class Connect : Object, Extensibility.IDTExtensibility2
	{
		/// <summary>
		///		Implements the constructor for the Add-in object.
		///		Place your initialization code within this method.
		/// </summary>
		public Connect()
		{
		}

		/// <summary>
		///      Implements the OnConnection method of the IDTExtensibility2 interface.
		///      Receives notification that the Add-in is being loaded.
		/// </summary>
		/// <param term='application'>
		///      Root object of the host application.
		/// </param>
		/// <param term='connectMode'>
		///      Describes how the Add-in is being loaded.
		/// </param>
		/// <param term='addInInst'>
		///      Object representing this Add-in.
		/// </param>
		/// <seealso class='IDTExtensibility2' />
		public void OnConnection(object application, Extensibility.ext_ConnectMode connectMode, object addInInst, ref System.Array custom)
		{
			_applicationObject = (_DTE)application;
			_addInInstance = (AddIn)addInInst;
			
			try
			{
				ResourceManager resource_manager = new ResourceManager("Vs71Win7x64PdbLockPatch.CommandBar", Assembly.GetExecutingAssembly());
				CultureInfo culture_info = new System.Globalization.CultureInfo(_applicationObject.LocaleID);
				string resource_name = String.Concat(culture_info.Parent.Name, "Debug");
				string debug_name =  resource_manager.GetString(resource_name);

				// We want our output in the Build window
				OutputWindow outputWindow = 
					(OutputWindow)_applicationObject.Windows.Item(Constants.vsWindowKindOutput).Object;
				output_window_pane = outputWindow.OutputWindowPanes.Item(debug_name);
				// Add ourselves as a OnBuildBegin/OnBuildDone handler
				EnvDTE.Events events = _applicationObject.Events;
				debugger_events = (EnvDTE.DebuggerEvents)events.DebuggerEvents;
			
				debugger_events.OnContextChanged += new _dispDebuggerEvents_OnContextChangedEventHandler(this.OnDbgContextChanged);
				debugger_events.OnEnterBreakMode += new _dispDebuggerEvents_OnEnterBreakModeEventHandler(this.OnDbgEnterBreakMode);
				debugger_events.OnEnterDesignMode += new _dispDebuggerEvents_OnEnterDesignModeEventHandler(this.OnDbgEnterDesignMode);
				debugger_events.OnEnterRunMode += new _dispDebuggerEvents_OnEnterRunModeEventHandler(this.OnDbgEnterRunMode);
				debugger_events.OnExceptionNotHandled += new _dispDebuggerEvents_OnExceptionNotHandledEventHandler(this.OnDbgExceptionNotHandled);
				debugger_events.OnExceptionThrown += new _dispDebuggerEvents_OnExceptionThrownEventHandler(this.OnDbgExceptionThrown);
			}
			catch (System.Exception ex)
			{
				MessageBox.Show(ex.Message, _addInInstance.ProgID);
			}
			
		}

		/// <summary>
		///     Implements the OnDisconnection method of the IDTExtensibility2 interface.
		///     Receives notification that the Add-in is being unloaded.
		/// </summary>
		/// <param term='disconnectMode'>
		///      Describes how the Add-in is being unloaded.
		/// </param>
		/// <param term='custom'>
		///      Array of parameters that are host application specific.
		/// </param>
		/// <seealso class='IDTExtensibility2' />
		public void OnDisconnection(Extensibility.ext_DisconnectMode disconnectMode, ref System.Array custom)
		{
			// Remove ourselves as a OnBuildBegin/OnBuildEnd handler
			if (debugger_events != null)
			{
				
			}
		}

		/// <summary>
		///      Implements the OnAddInsUpdate method of the IDTExtensibility2 interface.
		///      Receives notification that the collection of Add-ins has changed.
		/// </summary>
		/// <param term='custom'>
		///      Array of parameters that are host application specific.
		/// </param>
		/// <seealso class='IDTExtensibility2' />
		public void OnAddInsUpdate(ref System.Array custom)
		{
		}

		/// <summary>
		///      Implements the OnStartupComplete method of the IDTExtensibility2 interface.
		///      Receives notification that the host application has completed loading.
		/// </summary>
		/// <param term='custom'>
		///      Array of parameters that are host application specific.
		/// </param>
		/// <seealso class='IDTExtensibility2' />
		public void OnStartupComplete(ref System.Array custom)
		{
		}

		/// <summary>
		///      Implements the OnBeginShutdown method of the IDTExtensibility2 interface.
		///      Receives notification that the host application is being unloaded.
		/// </summary>
		/// <param term='custom'>
		///      Array of parameters that are host application specific.
		/// </param>
		/// <seealso class='IDTExtensibility2' />
		public void OnBeginShutdown(ref System.Array custom)
		{
		}
		
		private _DTE _applicationObject;
		private AddIn _addInInstance;
		private OutputWindowPane output_window_pane;
		private EnvDTE.DebuggerEvents debugger_events;
		

		public void OnDbgContextChanged(Process NewProcess, Program NewProgram, Thread NewThread, StackFrame NewStackFrame)
		{
			output_window_pane.OutputString("[vs71 win7x64 pdb lock patch]:OnDbgContextChanged\n");
		}

		public void OnDbgEnterBreakMode(dbgEventReason Reason, ref dbgExecutionAction ExecutionAction)
		{
			output_window_pane.OutputString("[vs71 win7x64 pdb lock patch]:OnDbgEnterBreakMode\n");
		}

		public void OnDbgEnterDesignMode(dbgEventReason Reason)
		{
			output_window_pane.OutputString("[vs71 win7x64 pdb lock patch]:OnDbgEnterDesignMode\n");

			try
			{
				UnlockFiles.UnlockDevenvPdb(output_window_pane);
			}
			catch (System.Exception ex)
			{
				MessageBox.Show(ex.Message, _addInInstance.ProgID);
			}
		}

		public void OnDbgEnterRunMode(dbgEventReason Reason)
		{
			output_window_pane.OutputString("[vs71 win7x64 pdb lock patch]:OnDbgEnterRunMode\n");
		}

		public void OnDbgExceptionNotHandled(string ExceptionType,
			string Name, int Code, string Description, ref dbgExceptionAction ExceptionAction)
		{
			output_window_pane.OutputString("[vs71 win7x64 pdb lock patch]:OnDbgExceptionNotHandled\n");
		}

		public void OnDbgExceptionThrown(string ExceptionType,
			string Name, int Code, string Description, ref dbgExceptionAction ExceptionAction)
		{
			output_window_pane.OutputString("[vs71 win7x64 pdb lock patch]:OnDbgExceptionThrown\n");
		}

	}
}