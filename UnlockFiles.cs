using System;
using System.Diagnostics;
using System.Reflection;
using System.IO;

namespace Vs71Win7x64PdbLockPatch
{
	/// <summary>
	/// Summary description for UnlockFile
	/// </summary>
	public class UnlockFiles
	{
		public static void UnlockDevenvPdb()
		{
			Assembly assembly_exe = Assembly.GetExecutingAssembly();
			DirectoryInfo assembly_path_info = new DirectoryInfo(assembly_exe.Location);
			DirectoryInfo assembly_dir_info = assembly_path_info.Parent;

			string hoho_pdb_unlocker_path = Path.Combine(assembly_dir_info.FullName, "HohoPdbUnlocker.exe");

			Process process = Process.GetCurrentProcess();

			Process unlock_process = new Process();
			unlock_process.StartInfo.UseShellExecute = false;
			unlock_process.StartInfo.RedirectStandardOutput = true;
			unlock_process.StartInfo.FileName = hoho_pdb_unlocker_path;
			unlock_process.StartInfo.CreateNoWindow = true;
			unlock_process.StartInfo.Arguments = string.Format("{0}", process.Id);
			unlock_process.Start();
			unlock_process.WaitForExit();

			string output = unlock_process.StandardOutput.ReadToEnd();
		}

		public static void UnlockDevenvPdb(EnvDTE.OutputWindowPane output_window_pane)
		{
			Assembly assembly_exe = Assembly.GetExecutingAssembly();
			DirectoryInfo assembly_path_info = new DirectoryInfo(assembly_exe.Location);
			DirectoryInfo assembly_dir_info = assembly_path_info.Parent;

			string hoho_pdb_unlocker_path = Path.Combine(assembly_dir_info.FullName, "HohoPdbUnlocker.exe");

			Process process = Process.GetCurrentProcess();

			Process unlock_process = new Process();
			unlock_process.StartInfo.UseShellExecute = false;
			unlock_process.StartInfo.RedirectStandardOutput = true;
			unlock_process.StartInfo.FileName = hoho_pdb_unlocker_path;
			unlock_process.StartInfo.CreateNoWindow = true;
			unlock_process.StartInfo.Arguments = string.Format("{0}", process.Id);
			unlock_process.Start();
			unlock_process.WaitForExit();

			string output = unlock_process.StandardOutput.ReadToEnd();

			output_window_pane.OutputString(output);
		}	
	}
}
